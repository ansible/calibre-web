# Calibre-Web

Calibre-Web is a fantastic way to browse and read our Calibre library.
It also works great as OPDS library for various mobile apps.
It is also easy to configure different user accounts with Calibre-Web.


## Smolk i glädjebägaren - no support for Keycloak or generic OAuth2

The project owner is vocally opposed to any changes to his existing (Github/Google)
oauth2 providers. I think he is unnecessarily rigid.

+ https://github.com/janeczku/calibre-web/pull/2211
+ https://github.com/janeczku/calibre-web/issues/2199
+ https://github.com/janeczku/calibre-web/issues/1140
+ https://git.coopcloud.tech/coop-cloud-chaos-patchs/calibre-web/issues/1


## Does `app.db` survive installation of new release?

Yes, it does. But:

> `CALIBRE_PORT` cannot be reset without wiping the `app.db`
> https://github.com/janeczku/calibre-web/issues/1025#issuecomment-524818660


## Inspiration

+ https://input.sh/how-i-manage-my-ebooks/


## Refs

+ https://github.com/janeczku/calibre-web
